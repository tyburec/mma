////////////////////////////////////////////////////////////////////////////////
// Copyright © 2018 Jérémie Dumas
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
////////////////////////////////////////////////////////////////////////////////

#include "MMASolver.h"

#ifdef MMA_PROFILE
#include "profile.cpp"
#endif

#include <algorithm>
#include <cmath>
#include <cstdio>
#include <iostream>

////////////////////////////////////////////////////////////////////////////////
// PUBLIC
////////////////////////////////////////////////////////////////////////////////

MMASolver::MMASolver(int nn, int mm, double ai, double ci, double di)
    : n(nn), m(mm), iter(0), xmamieps(1.0e-5)
      //, epsimin(1e-7)
      ,
      epsimin(std::sqrt(n + m) * 1e-9), raa0(0.00001), move(0.5), albefa(0.1), asyminit(0.5)    // 0.2;
      ,
      asymdec(0.7)    // 0.65;
      ,
      asyminc(1.2)    // 1.08;
      ,
      a(m, ai), c(m, ci), d(m, di), y(m), lam(m), mu(m), s(2 * m), low(n), upp(n), alpha(n), beta(n), p0(n), q0(n), pij(n * m), qij(n * m), b(m), grad(m), hess(m * m), xold1(n), xold2(n)
{}

void MMASolver::SetA(std::vector<double> apar)
{
    if (apar.size() != (size_t)this->m)
        throw std::range_error("MMASolver::SetA: A parameter can not ve overwritten. Wrong size.");

    this->a = apar;
}

void MMASolver::SetC(std::vector<double> cpar)
{
    if (cpar.size() != (size_t)this->m)
        throw std::range_error("MMASolver::SetC: C parameter can not ve overwritten. Wrong size.");

    this->c = cpar;
}

void MMASolver::SetD(std::vector<double> dpar)
{
    if (dpar.size() != (size_t)this->m)
        throw std::range_error("MMASolver::SetD: D parameter can not ve overwritten. Wrong size.");

    this->d = dpar;
}

auto MMASolver::GetM() const -> double
{
    return this->m;
}

auto MMASolver::GetN() const -> double
{
    return this->n;
}

auto MMASolver::GetA() const -> std::vector<double>
{
    return this->a;
}

auto MMASolver::GetC() const -> std::vector<double>
{
    return this->c;
}

auto MMASolver::GetD() const -> std::vector<double>
{
    return this->d;
}

void MMASolver::SetAsymptotes(double init, double decrease, double increase)
{

    // asymptotes initialization and increase/decrease
    asyminit = init;
    asymdec = decrease;
    asyminc = increase;
}

void MMASolver::Update(double* xval, const double* dfdx, const double* gx, const double* dgdx,
    const double* xmin, const double* xmax)
{

    // Generate the subproblem
#ifdef MMA_PROFILE
    auto sw = stopwatch();
#endif
    GenSub(xval, dfdx, gx, dgdx, xmin, xmax);
#ifdef MMA_PROFILE
    std::cout << "GenSub: " << sw.elapsed() << "s." << std::endl;
#endif

    // Update xolds
    xold2 = xold1;
    std::copy_n(xval, n, xold1.data());

    // Solve the dual with an interior point method
#ifdef MMA_PROFILE
    sw.elapsed();
#endif
    SolveDIP(xval);
#ifdef MMA_PROFILE
    std::cout << "SolveDIP: " << sw.elapsed() << "s." << std::endl;
#endif
    // Solve the dual with a steepest ascent method
    // SolveDSA(xval);
}

////////////////////////////////////////////////////////////////////////////////
// PRIVATE
////////////////////////////////////////////////////////////////////////////////

#ifdef MMA_WITH_OPENMP
#ifdef OPENMP45
#pragma omp declare reduction(vec_double_plus                                                                                          \
                              : std::vector <double>                                                                                   \
                              : std::transform(omp_out.begin(), omp_out.end(), omp_in.begin(), omp_out.begin(), std::plus <double>())) \
    initializer(omp_priv = decltype(omp_orig)(omp_orig.size()))
#endif
#endif

void MMASolver::SolveDIP(double* x)
{
    const double tol = epsimin;    // 1.0e-9*sqrt(m+n);
    double epsi = 1.0;
    double err = 1.0;
    int loop;
    double* df2 = new double[n];
    double* PQ = new double[n * m];
    double* tmp = new double[n * m];
    //double* res = new double[2 * m];
    std::vector<double> res(2*m);

#ifdef MMA_PROFILE
    auto tXYZ = double{0};
    auto nXYZ = size_t{0};
    auto tdgrad = double{0};
    auto ndgrad = size_t{0};
    auto thess = double{0};
    auto nhess = size_t{0};
    auto tfact = double{0};
    auto nfact = size_t{0};
    auto tsolve = double{0};
    auto nsolve = size_t{0};
    auto tdualls = double{0};
    auto ndualls = size_t{0};
    auto tresidual = double{0};
    auto nresidual = size_t{0};
#endif

    for (int j = 0; j < m; ++j) {
        lam[j] = c[j] / 2.0;
        mu[j] = 1.0;
    }

    while (epsi > tol) {

        loop = 0;
        while (err > 0.9 * epsi && loop < 100) {
            loop++;

// Set up Newton system
#ifdef MMA_PROFILE
            auto sw = stopwatch();
#endif
            XYZofLAMBDA(x);
#ifdef MMA_PROFILE
            nXYZ++;
            tXYZ += sw.elapsed();
            sw.reset();
#endif
            DualGrad(x);
#ifdef MMA_PROFILE
            ndgrad++;
            tdgrad += sw.elapsed();
#endif
            for (int j = 0; j < m; j++)
                grad[j] = -1.0 * grad[j] - epsi / lam[j];
#ifdef MMA_PROFILE
            sw.reset();
#endif
            DualHess(x, df2, PQ, tmp);
#ifdef MMA_PROFILE
            nhess++;
            thess += sw.elapsed();
#endif
            // Solve Newton system
            if (m > 1) {
#ifdef MMA_PROFILE
                sw.reset();
#endif
                Factorize(hess.data(), m);
#ifdef MMA_PROFILE
                nfact++;
                tfact += sw.elapsed();
                sw.reset();
#endif
                Solve(hess.data(), grad.data(), m);
#ifdef MMA_PROFILE
                nsolve++;
                tsolve += sw.elapsed();
#endif
                for (int j = 0; j < m; j++)
                    s[j] = grad[j];
            }
            else if (m > 0)
                s[0] = grad[0] / hess[0];

            // Get the full search direction
            for (int i = 0; i < m; i++)
                s[m + i] = -mu[i] + epsi / lam[i] - s[i] * mu[i] / lam[i];

                // Perform linesearch and update lam and mu
#ifdef MMA_PROFILE
            sw.reset();
#endif
            DualLineSearch();
#ifdef MMA_PROFILE
            ndualls++;
            tdualls += sw.elapsed();
            sw.reset();
#endif
            XYZofLAMBDA(x);
#ifdef MMA_PROFILE
            nXYZ++;
            tXYZ += sw.elapsed();

            // Compute KKT res
            sw.reset();
#endif
            err = DualResidual(x, epsi, res);
#ifdef MMA_PROFILE
            nresidual++;
            tresidual += sw.elapsed();
#endif
        }
        epsi = epsi * 0.1;
    }

    delete[] df2;
    delete[] PQ;
    delete[] tmp;
    //delete[] res;

#ifdef MMA_PROFILE
    std::cout << "XYZ: " << nXYZ << " " << tXYZ << "s." << std::endl;
    std::cout << "dgrad: " << ndgrad << " " << tdgrad << "s." << std::endl;
    std::cout << "hess: " << nhess << " " << thess << "s." << std::endl;
    std::cout << "fact: " << nfact << " " << tfact << "s." << std::endl;
    std::cout << "solve: " << nsolve << " " << tsolve << "s." << std::endl;
    std::cout << "dualls: " << ndualls << " " << tdualls << "s." << std::endl;
    std::cout << "residual: " << nresidual << " " << tresidual << "s." << std::endl;
#endif
}

void MMASolver::SolveDSA(double* x)
{
    const double tol = epsimin;    // 1.0e-9*sqrt(m+n);
    double err = 1.0;
    int loop = 0;

    for (int j = 0; j < m; j++)
        lam[j] = 1.0;

    while (err > tol && loop < 500) {
        loop++;
        XYZofLAMBDA(x);
        DualGrad(x);
        double theta = 1.0;
        err = 0.0;
        for (int j = 0; j < m; j++) {
            lam[j] = std::max(0.0, lam[j] + theta * grad[j]);
            err += grad[j] * grad[j];
        }
        err = std::sqrt(err);
    }
}

double MMASolver::DualResidual(double* x, double epsi, std::vector<double>& res)
{
    auto m2 = 2 * m;
    auto nrI = double{0.0};

#ifdef MMA_WITH_OPENMP
#pragma omp parallel
#endif
    {
#ifdef MMA_WITH_OPENMP
#pragma omp for nowait
#endif
        for (int j = 0; j < m; j++)
            res[j] = -b[j] - a[j] * z - y[j] + mu[j];
#ifdef MMA_WITH_OPENMP
#pragma omp for
#endif
        for (int j = 0; j < m; j++)
            res[j + m] = mu[j] * lam[j] - epsi;

#ifdef MMA_WITH_OPENMP
#ifdef OPENMP45
#pragma omp for reduction(vec_double_plus \
                          : res)
#else
#pragma omp single
#endif
#endif
        for (int i = 0; i < n; i++)
            for (int j = 0; j < m; j++)
                res[j] += pij[i * m + j] / (upp[i] - x[i]) + qij[i * m + j] / (x[i] - low[i]);

#ifdef MMA_WITH_OPENMP
#pragma omp for reduction(max \
                          : nrI)
#endif
        for (int i = 0; i < 2 * m; i++)
            nrI = std::max(nrI, std::abs(res[i]));
    }
    return nrI;
}

void MMASolver::DualLineSearch()
{
    double theta = 1.005;

    for (int i = 0; i < m; i++) {
        theta = std::max(theta, -1.01 * s[i] / lam[i]);
        theta = std::max(theta, -1.01 * s[i + m] / mu[i]);
    }

    theta = 1.0 / theta;

    for (int i = 0; i < m; i++) {
        lam[i] += theta * s[i];
        mu[i] += theta * s[i + m];
    }
}

void MMASolver::DualHess(double* x, double*& df2, double*& PQ, double*& tmp)
{

    double lamai = 0.0;
    double HessTrace = 0.0;
    double HessCorr = 0.0;

#ifdef MMA_WITH_OPENMP
#pragma omp parallel
#endif
    {
#ifdef MMA_WITH_OPENMP
#pragma omp for
#endif
        for (int i = 0; i < n; i++) {
            double pjlam = p0[i];
            double qjlam = q0[i];
            for (int j = 0; j < m; j++) {
                pjlam += pij[i * m + j] * lam[j];
                qjlam += qij[i * m + j] * lam[j];
                PQ[i * m + j] = pij[i * m + j] / pow(upp[i] - x[i], 2.0) - qij[i * m + j] / pow(x[i] - low[i], 2.0);
            }
            df2[i] = -1.0 / (2.0 * pjlam / pow(upp[i] - x[i], 3.0) + 2.0 * qjlam / pow(x[i] - low[i], 3.0));
            double xp = (sqrt(pjlam) * low[i] + sqrt(qjlam) * upp[i]) / (sqrt(pjlam) + sqrt(qjlam));
            if (xp < alpha[i])
                df2[i] = 0.0;
            if (xp > beta[i])
                df2[i] = 0.0;
        }

#ifdef MMA_WITH_OPENMP
#pragma omp for nowait
#endif
        for (int i = 0; i < m; i++)
            for (int j = 0; j < m; j++)
                hess[i * m + j] = 0.0;

                // Create the matrix/matrix/matrix product: PQ^T * diag(df2) * PQ
#ifdef MMA_WITH_OPENMP
#pragma omp for
#endif
        for (int i = 0; i < n; i++)
            for (int j = 0; j < m; j++)
                tmp[j * n + i] = PQ[i * m + j] * df2[i];

#ifdef MMA_WITH_OPENMP
#ifdef OPENMP45
#pragma omp for reduction(vec_double_plus \
                          : hess)
#else
#pragma omp single
#endif
#endif
        for (int k = 0; k < n; k++)
            for (int i = 0; i < m; i++)
                for (int j = 0; j < m; j++)
                    hess[i * m + j] += tmp[i * n + k] * PQ[k * m + j];

#ifdef MMA_WITH_OPENMP
#pragma omp for
#endif
        for (int j = 0; j < m; j++) {
            if (lam[j] < 0.0)
                lam[j] = 0.0;
            if (lam[j] > c[j])
                hess[j * m + j] += -1.0;
            hess[j * m + j] += -mu[j] / lam[j];
        }

#ifdef MMA_WITH_OPENMP
#pragma omp for reduction(+ \
                          : lamai)
#endif
        for (int j = 0; j < m; ++j)
            lamai += lam[j] * a[j];

        if (lamai > 0.0)
#ifdef MMA_WITH_OPENMP
#pragma omp for
#endif
            for (int j = 0; j < m; j++)
                for (int k = 0; k < m; k++)
                    hess[j * m + k] += -10.0 * a[j] * a[k];

// pos def check
#ifdef MMA_WITH_OPENMP
#pragma omp for reduction(+ \
                          : HessTrace)
#endif
        for (int i = 0; i < m; i++)
            HessTrace += hess[i * m + i];

#ifdef MMA_WITH_OPENMP
#pragma omp single
#endif
        {
            HessCorr = 1e-4 * HessTrace / m;

            if (-1.0 * HessCorr < 1.0e-7) {
                HessCorr = -1.0e-7;
            }
        }

#ifdef MMA_WITH_OPENMP
#pragma omp for
#endif
        for (int i = 0; i < m; i++)
            hess[i * m + i] += HessCorr;
    }
}

void MMASolver::DualGrad(double* x)
{
#ifdef MMA_WITH_OPENMP
#pragma omp parallel
#endif
    {
#ifdef MMA_WITH_OPENMP
#pragma omp for
#endif
        for (int j = 0; j < m; ++j)
            grad[j] = -b[j] - a[j] * z - y[j];

#ifdef MMA_WITH_OPENMP
#ifdef OPENMP45
#pragma omp for reduction(vec_double_plus \
                          : grad)
#else
#pragma omp single
#endif
#endif
        for (int i = 0; i < n; i++)
            for (int j = 0; j < m; j++)
                grad[j] += pij[i * m + j] / (upp[i] - x[i]) + qij[i * m + j] / (x[i] - low[i]);
    }
}

void MMASolver::XYZofLAMBDA(double* x)
{
    double lamai = 0.0;

#ifdef MMA_WITH_OPENMP
#pragma omp parallel
#endif
    {
#ifdef MMA_WITH_OPENMP
#pragma omp for reduction(+ \
                          : lamai)
#endif
        for (int i = 0; i < m; i++) {
            if (lam[i] < 0.0)
                lam[i] = 0;
            y[i] = std::max(0.0, lam[i] - c[i]);    // Note y=(lam-c)/d - however d is fixed at one !!
            lamai += lam[i] * a[i];
        }

#ifdef MMA_WITH_OPENMP
#pragma omp single
#endif
        {
            z = std::max(0.0, 10.0 * (lamai - 1.0));    // SINCE a0 = 1.0
        }

#ifdef MMA_WITH_OPENMP
#pragma omp for
#endif
        for (int i = 0; i < n; i++) {
            double pjlam = p0[i];
            double qjlam = q0[i];
            for (int j = 0; j < m; j++) {
                pjlam += pij[i * m + j] * lam[j];
                qjlam += qij[i * m + j] * lam[j];
            }
            x[i] = (sqrt(pjlam) * low[i] + sqrt(qjlam) * upp[i]) / (sqrt(pjlam) + sqrt(qjlam));
            if (x[i] < alpha[i])
                x[i] = alpha[i];
            if (x[i] > beta[i])
                x[i] = beta[i];
        }
    }
}

void MMASolver::GenSub(const double* xval, const double* dfdx, const double* gx, const double* dgdx, const double* xmin,
    const double* xmax)
{
    // Forward the iterator
    iter++;

// Set asymptotes
#ifdef MMA_WITH_OPENMP
#pragma omp parallel
#endif
    {
        if (iter < 3) {
#ifdef MMA_WITH_OPENMP
#pragma omp for
#endif
            for (int i = 0; i < n; i++) {
                low[i] = xval[i] - asyminit * (xmax[i] - xmin[i]);
                upp[i] = xval[i] + asyminit * (xmax[i] - xmin[i]);
            }
        }
        else {
#ifdef MMA_WITH_OPENMP
#pragma omp for
#endif
            for (int i = 0; i < n; i++) {
                double zzz = (xval[i] - xold1[i]) * (xold1[i] - xold2[i]);
                double gamma{1.0};
                if (zzz < 0.0)
                    gamma = asymdec;
                else if (zzz > 0.0)
                    gamma = asyminc;
                low[i] = xval[i] - gamma * (xold1[i] - low[i]);
                upp[i] = xval[i] + gamma * (upp[i] - xold1[i]);

                double xmami = std::max(xmamieps, xmax[i] - xmin[i]);
                // double xmami = xmax[i] - xmin[i];
                low[i] = std::max(low[i], xval[i] - 100.0 * xmami);
                low[i] = std::min(low[i], xval[i] - 1.0e-5 * xmami);
                upp[i] = std::max(upp[i], xval[i] + 1.0e-5 * xmami);
                upp[i] = std::min(upp[i], xval[i] + 100.0 * xmami);

                double xmi = xmin[i] - 1.0e-6;
                double xma = xmax[i] + 1.0e-6;
                if (xval[i] < xmi) {
                    low[i] = xval[i] - (xma - xval[i]) / 0.9;
                    upp[i] = xval[i] + (xma - xval[i]) / 0.9;
                }
                if (xval[i] > xma) {
                    low[i] = xval[i] - (xval[i] - xmi) / 0.9;
                    upp[i] = xval[i] + (xval[i] - xmi) / 0.9;
                }
            }
        }

// Set bounds and the coefficients for the approximation
//double raa0 = 0.5*1e-6;
#ifdef MMA_WITH_OPENMP
#pragma omp for nowait
#endif
        for (int i = 0; i < n; ++i) {
            // Compute bounds alpha and beta
            alpha[i] = std::max(xmin[i], low[i] + albefa * (xval[i] - low[i]));
            alpha[i] = std::max(alpha[i], xval[i] - move * (xmax[i] - xmin[i]));
            alpha[i] = std::min(alpha[i], xmax[i]);
            beta[i] = std::min(xmax[i], upp[i] - albefa * (upp[i] - xval[i]));
            beta[i] = std::min(beta[i], xval[i] + move * (xmax[i] - xmin[i]));
            beta[i] = std::max(beta[i], xmin[i]);
        }

        // Objective function
#ifdef MMA_WITH_OPENMP
#pragma omp for nowait
#endif
        for (int i = 0; i < n; ++i) {
            double dfdxp = std::max(0.0, dfdx[i]);
            double dfdxm = std::max(0.0, -1.0 * dfdx[i]);
            double xmamiinv = 1.0 / std::max(xmamieps, xmax[i] - xmin[i]);
            double pq = 0.001 * std::abs(dfdx[i]) + raa0 * xmamiinv;
            p0[i] = std::pow(upp[i] - xval[i], 2.0) * (dfdxp + pq);
            q0[i] = std::pow(xval[i] - low[i], 2.0) * (dfdxm + pq);
        }

#ifdef MMA_WITH_OPENMP
#pragma omp for nowait
#endif
        for (int j = 0; j < m; j++)
            b[j] = -gx[j];

            // Constraints
#ifdef MMA_WITH_OPENMP
#pragma omp for
#endif
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < m; j++) {
                double dgdxp = std::max(0.0, dgdx[i * m + j]);
                double dgdxm = std::max(0.0, -1.0 * dgdx[i * m + j]);
                double xmamiinv = 1.0 / std::max(xmamieps, xmax[i] - xmin[i]);
                double pq = 0.001 * std::abs(dgdx[i * m + j]) + raa0 * xmamiinv;
                pij[i * m + j] = std::pow(upp[i] - xval[i], 2.0) * (dgdxp + pq);
                qij[i * m + j] = std::pow(xval[i] - low[i], 2.0) * (dgdxm + pq);
            }
        }

        // The constant for the constraints
#ifdef MMA_WITH_OPENMP
#ifdef OPENMP45
#pragma omp for reduction(vec_double_plus \
                          : b)
#else
#pragma omp single
#endif
#endif
        for (int i = 0; i < n; i++)
            for (int j = 0; j < m; j++)
                b[j] += pij[i * m + j] / (upp[i] - xval[i]) + qij[i * m + j] / (xval[i] - low[i]);
    }
}

void MMASolver::Factorize(double* K, int n)
{

    for (int s = 0; s < n - 1; s++)
        for (int i = s + 1; i < n; i++) {
            K[i * n + s] /= K[s * n + s];
            for (int j = s + 1; j < n; j++)
                K[i * n + j] -= K[i * n + s] * K[s * n + j];
        }
}

void MMASolver::Solve(double* K, double* x, int n)
{
    for (int i = 1; i < n; i++) {
        double a = 0.0;
        for (int j = 0; j < i; j++)
            a -= K[i * n + j] * x[j];
        x[i] += a;
    }

    x[n - 1] = x[n - 1] / K[(n - 1) * n + (n - 1)];
    for (int i = n - 2; i >= 0; i--) {
        double a = x[i];
        for (int j = i + 1; j < n; j++)
            a -= K[i * n + j] * x[j];
        x[i] = a / K[i * n + i];
    }
}
