#include <chrono>

class stopwatch
{
private:
    std::chrono::time_point<std::chrono::steady_clock> start_time;

public:
    stopwatch()
    {
        this->start_time = std::chrono::steady_clock::now();
    }

    void reset()
    {
        start_time = std::chrono::steady_clock::now();
    };

    auto elapsed() -> double
    {
        auto curTime = std::chrono::steady_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::duration<double>>(curTime - start_time);
        return duration.count();
    };
};